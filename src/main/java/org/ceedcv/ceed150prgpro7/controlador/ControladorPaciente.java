package org.ceedcv.ceed150prgpro7.controlador;


import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.*;
import javax.swing.event.DocumentListener;
import org.ceedcv.ceed150prgpro7.modelo.IModelo;
import org.ceedcv.ceed150prgpro7.modelo.Paciente;
import org.ceedcv.ceed150prgpro7.vista.*;




public class ControladorPaciente implements ActionListener{

   
    VistaGraficaPaciente vistapaciente;
    IModelo modelo;
    String opcion;
    ArrayList pacientes;
    Paciente activo;
    int contActivo;     
    
    ControladorPaciente(VistaGraficaPaciente vistapaciente, IModelo modelo) throws IOException {
        this.vistapaciente = vistapaciente;
        this.modelo = modelo;
        
      
        vistapaciente.CrearMenuPaciente();
        
        //inicializar botones
        this.vistapaciente.ventana.setVisible(true);
        this.vistapaciente.botonCreate.addActionListener(this);
        this.vistapaciente.botonRead.addActionListener(this);
        this.vistapaciente.botonUpdate.addActionListener(this);
        this.vistapaciente.botonDelete.addActionListener(this);
        this.vistapaciente.botonCancelar.addActionListener(this);
        this.vistapaciente.botonAceptar.addActionListener(this);
        this.vistapaciente.botonSalir.addActionListener(this);
        this.vistapaciente.botonGuardar.addActionListener(this);
        this.vistapaciente.botonBorrar.addActionListener(this);
        this.vistapaciente.botonPrimero.addActionListener(this);
        this.vistapaciente.botonUltimo.addActionListener(this);
        this.vistapaciente.botonSiguiente.addActionListener(this);
        this.vistapaciente.botonAnterior.addActionListener(this);
                
        editarCampos(Boolean.valueOf(false));
        activarBotones(Boolean.valueOf(true));
       
        
        this.pacientes = this.modelo.readp();
        if (this.pacientes.size() > 0) {
          primero();
          mostrar(this.activo);
        }
    }

    public void actionPerformed(ActionEvent e) {
        Object evento = e.getSource();
        if (this.vistapaciente.botonPrimero == evento){
            primero();
            mostrar(this.activo);
        }
        else if (this.vistapaciente.botonAnterior == evento){
            anterior();
            mostrar(this.activo);
        }
        else if (this.vistapaciente.botonSiguiente == evento){
            siguiente();
            mostrar(this.activo);
        }
        else if (this.vistapaciente.botonUltimo == evento){
            ultimo();
            mostrar(this.activo);
        }
        else if(this.vistapaciente.botonCreate == evento){
                 
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistapaciente.txtNombre.requestFocus();
            vaciarCampos();
            editarCampos(Boolean.valueOf(true));
            vistapaciente.txtId.setEditable(false);
            activarBotones(Boolean.valueOf(false));
            VerificarCampos();
            this.opcion = "create";
        }else if (vistapaciente.botonRead == evento){
            
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistapaciente.txtId.requestFocus();
            vaciarCampos();
            vistapaciente.txtId.setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "read";   
                
        }else if(vistapaciente.botonUpdate == evento){
            
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistapaciente.txtId.requestFocus();
            vaciarCampos();
            vistapaciente.txtId.setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "update";  
                
        }else if(vistapaciente.botonDelete == evento){
            
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistapaciente.txtId.requestFocus();
            vaciarCampos();
            vistapaciente.txtId.setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "delete";  
          
        }else if (vistapaciente.botonCancelar == evento){
            
            vaciarCampos();
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            primero();
           
        }else if (vistapaciente.botonAceptar == evento){
            menuAceptar(opcion);
            
        }else if (vistapaciente.botonGuardar == evento){
            
            Paciente paciente = new Paciente();
            paciente = obtener();
            modelo.update(paciente);
            pacientes = modelo.readp();
            
            vistapaciente.botonGuardar.setVisible(false);
            vistapaciente.botonAceptar.setVisible(true);
            
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            this.activo = paciente;
        }else if (vistapaciente.botonBorrar == evento){
            
            Paciente paciente = new Paciente();
            paciente = obtener();
            modelo.delete(paciente);
            pacientes = modelo.readp();
            vaciarCampos();
            vistapaciente.botonBorrar.setVisible(false);
            vistapaciente.botonAceptar.setVisible(true);
            
            anterior();
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            
        }else if (vistapaciente.botonSalir == evento){
            
            this.vistapaciente.ventana.setVisible(false);
            
        }
    }
    public void menuAceptar(String opcion) {
        JFrame frame = new JFrame();
        switch(opcion){
            case "create":
                if(//!vistapaciente.txtId.getText().equals("") && 
                   !vistapaciente.txtNombre.getText().equals("") && 
                   !vistapaciente.txtDni.getText().equals("") && 
                   !vistapaciente.txtNss.getText().equals("") && 
                   !vistapaciente.txtEdad.getText().equals("") && 
                   !vistapaciente.txtTelefono.getText().equals("")){
                        Paciente paciente = obtener();
                        modelo.create(paciente);
                        pacientes.add(paciente);
                        mostrar(paciente);
                        editarCampos(Boolean.valueOf(false));
                        activarBotones(Boolean.valueOf(true));
                    this.activo = paciente;

                }else JOptionPane.showMessageDialog(frame, "ERROR: Completa todos los datos", "Mensaje de Error", JOptionPane.WARNING_MESSAGE);
                break;
            case "read":
               if (!vistapaciente.txtId.getText().equals("")){
                  getPaciente(Integer.parseInt(vistapaciente.txtId.getText()));
                             
                 }
               activarBotones(Boolean.valueOf(false));
               break;
            case "update":
                if (!vistapaciente.txtId.getText().equals("")){
                  getPaciente(Integer.parseInt(vistapaciente.txtId.getText()));
                 }
                vistapaciente.botonGuardar.setVisible(true);
                editarCampos(Boolean.valueOf(true));
                vistapaciente.txtId.setEditable(false);
                activarBotones(Boolean.valueOf(false));
                vistapaciente.botonAceptar.setVisible(false);
                 break;
            case "delete":
                if (!vistapaciente.txtId.getText().equals("")){
                  getPaciente(Integer.parseInt(vistapaciente.txtId.getText()));
                }
                vistapaciente.botonBorrar.setVisible(true);
                editarCampos(Boolean.valueOf(false));
                vistapaciente.txtId.setEditable(false);
                activarBotones(Boolean.valueOf(false));
                vistapaciente.botonAceptar.setVisible(false);
                break;
           
        }
    }
       
    public void activarBotones(Boolean booleano){
        
        this.vistapaciente.botonCreate.setEnabled(booleano.booleanValue());
        this.vistapaciente.botonRead.setEnabled(booleano.booleanValue());
        this.vistapaciente.botonUpdate.setEnabled(booleano.booleanValue());
        this.vistapaciente.botonDelete.setEnabled(booleano.booleanValue());
        this.vistapaciente.botonCancelar.setEnabled(!booleano.booleanValue());
        this.vistapaciente.botonAceptar.setEnabled(!booleano.booleanValue());
    }
    
    public void editarCampos(Boolean booleano){
        
        this.vistapaciente.txtId.setEditable(booleano.booleanValue());
        this.vistapaciente.txtNombre.setEditable(booleano.booleanValue());
        this.vistapaciente.txtDni.setEditable(booleano.booleanValue());
        this.vistapaciente.txtNss.setEditable(booleano.booleanValue());
        this.vistapaciente.txtEdad.setEditable(booleano.booleanValue());
        this.vistapaciente.txtTelefono.setEditable(booleano.booleanValue());
        this.vistapaciente.txtObservaciones.setEditable(booleano.booleanValue());
        //this.vistapaciente.getObservaciones().setEditable(booleano.booleanValue());
              
    }
    
     public void vaciarCampos (){
        this.vistapaciente.txtId.setText("");
        this.vistapaciente.txtNombre.setText("");
        this.vistapaciente.txtDni.setText("");
        this.vistapaciente.txtNss.setText("");
        this.vistapaciente.txtEdad.setText("");
        this.vistapaciente.txtTelefono.setText("");
        this.vistapaciente.txtObservaciones.setText("");  
    }
     
        private Paciente obtener() {
       
            Paciente paciente = new Paciente();
            int edad, telefono, id;
                try{
                  id = Integer.parseInt(this.vistapaciente.txtId.getText());
                  paciente.setId(id);
                }catch (NumberFormatException ex){
                  //paciente.setId(0);
                }
            
            paciente.setNombre(this.vistapaciente.txtNombre.getText());
            paciente.setDni(this.vistapaciente.txtDni.getText());
            paciente.setNss(this.vistapaciente.txtNss.getText());
            
            if(this.vistapaciente.txtObservaciones.getText().equals("")){
                paciente.setObservaciones(" ");
                }else{
                     paciente.setObservaciones(this.vistapaciente.txtObservaciones.getText());
                } 
            
                try{
                  edad = Integer.parseInt(this.vistapaciente.txtEdad.getText());
                  paciente.setEdad(edad);
                }catch (NumberFormatException ex){
                  paciente.setEdad(0);
                }

                 try{
                  telefono =Integer.parseInt(this.vistapaciente.txtTelefono.getText());
                  paciente.setTelefono(telefono);
                }catch (NumberFormatException ex){
                  paciente.setTelefono(000000000);
                }

            return paciente;

    }

   
private void getPaciente(int id) {
                      
        Iterator iterator = modelo.readp().iterator();
        while (iterator.hasNext()) {                    
            Paciente paciente = (Paciente) iterator.next();
            if (id == paciente.getId()){                
                   //this.vistamedico.txtId.setText(medico.getId());
                this.vistapaciente.txtNombre.setText(paciente.getNombre());
                this.vistapaciente.txtDni.setText(paciente.getDni());
                this.vistapaciente.txtNss.setText(paciente.getNss());
                this.vistapaciente.txtEdad.setText(String.valueOf(paciente.getEdad()));
                this.vistapaciente.txtTelefono.setText(String.valueOf(paciente.getTelefono()));
                this.vistapaciente.txtObservaciones.setText(paciente.getObservaciones());
                this.activo = paciente;
            } 
               
        }
}
    private void esperarXsegundos(int segundos) {
	try {
            Thread.sleep(segundos * 1000);
	} catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
	}
    }

    private void primero() {
        
        if (this.pacientes != null){
            this.activo = ((Paciente)this.pacientes.get(0));
        } else{
            this.activo = null;
            this.contActivo = -1;
        }
               
    }   
    private void anterior() {
       if (this.contActivo != 0){
            this.contActivo -= 1;
            this.activo = ((Paciente)this.pacientes.get(this.contActivo));
        }
        
    }

    private void siguiente() {
        if (this.contActivo != this.pacientes.size() - 1){
            this.contActivo += 1;
            this.activo = ((Paciente)this.pacientes.get(this.contActivo));
        }
    }

    private void ultimo() {
        this.contActivo = (this.pacientes.size() - 1);
        this.activo = ((Paciente)this.pacientes.get(this.contActivo));
                 
    }
    
    private void mostrar(Paciente primero){
        
        editarCampos(Boolean.valueOf(false));
        activarBotones(Boolean.valueOf(true));
        if (primero == null) {
          return;
        } else {
            
            this.vistapaciente.txtId.setText(String.valueOf(primero.getId()));
            this.vistapaciente.txtNombre.setText(primero.getNombre());
            this.vistapaciente.txtDni.setText(primero.getDni());
            this.vistapaciente.txtNss.setText(primero.getNss());
            this.vistapaciente.txtEdad.setText(String.valueOf(primero.getEdad()));
            this.vistapaciente.txtTelefono.setText(String.valueOf(primero.getTelefono()));
            this.vistapaciente.txtObservaciones.setText(primero.getObservaciones());
        }
    }
    private void VerificarCampos(){
        this.vistapaciente.txtEdad.setInputVerifier(new VerificarCampos("txtEdad"));
        this.vistapaciente.txtDni.setInputVerifier(new VerificarCampos("txtDni"));
        this.vistapaciente.txtNss.setInputVerifier(new VerificarCampos("txtNss"));
        this.vistapaciente.txtTelefono.setInputVerifier(new VerificarCampos("txtTelefono"));
        this.vistapaciente.txtNombre.setInputVerifier(new VerificarCampos("txtNombre"));
    } 
}