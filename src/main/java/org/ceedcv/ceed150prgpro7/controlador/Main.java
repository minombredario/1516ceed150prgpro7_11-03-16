/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro7.controlador;

import java.io.IOException;
import org.ceedcv.ceed150prgpro7.modelo.IModelo;
import org.ceedcv.ceed150prgpro7.modelo.ModeloFichero;
import org.ceedcv.ceed150prgpro7.vista.VistaGraficaMenu;



/**
 * Fichero: main.java
 *
 * @author Darío Navarro Andrés minombredario@gmail.com
 * 
 */
public class Main {

    public static void main(String[] args) throws IOException {

        IModelo modelo = new ModeloFichero();

        //VistaMenu vista = new VistaMenu();
        VistaGraficaMenu menugrafica = new VistaGraficaMenu();

        //Controlador menu = new Controlador(modelo, vista);
        Controlador menu = new Controlador(modelo, menugrafica);
    }

}
