package org.ceedcv.ceed150prgpro7.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.ceedcv.ceed150prgpro7.modelo.*;
import org.ceedcv.ceed150prgpro7.vista.*;

/**
 * Fichero: ControladorCita.java
 * @author Darío Navarro Andrés minombredario@gmail.com
 * 
 */
public class ControladorCita implements ActionListener{
     
    VistaGraficaCita vistacita;
    IModelo modelo;
    String opcion;
    ArrayList citas;
    Cita activa;
    int contActiva;
    ModeloFichero mf;
    int id_medico, edad_medico, telefono_medico;
    String nombre_medico, dni_medico, colegiado, especialidad, datos_medico;
    int id_paciente, edad_paciente, telefono_paciente;
    String nombre_paciente, dni_paciente, nss, datos_paciente;
    
    public ControladorCita (VistaGraficaCita vistacita , IModelo modelo) throws IOException{
        
        this.vistacita = vistacita;
        this.modelo = modelo;
        
        vistacita.CrearMenuCita();
        this.vistacita.ventana.setVisible(true);
        this.vistacita.botonCreate.addActionListener(this);
        this.vistacita.botonRead.addActionListener(this);
        this.vistacita.botonUpdate.addActionListener(this);
        this.vistacita.botonDelete.addActionListener(this);
        this.vistacita.botonCancelar.addActionListener(this);
        this.vistacita.botonAceptar.addActionListener(this);
        this.vistacita.botonSalir.addActionListener(this);
        this.vistacita.botonGuardar.addActionListener(this);
        this.vistacita.botonBorrar.addActionListener(this);
        this.vistacita.botonPrimero.addActionListener(this);
        this.vistacita.botonUltimo.addActionListener(this);
        this.vistacita.botonSiguiente.addActionListener(this);
        this.vistacita.botonAnterior.addActionListener(this);
        this.vistacita.botonMedico.addActionListener(this);
        this.vistacita.botonPaciente.addActionListener(this);
        
              
        this.editarCampos(Boolean.valueOf(false));
        this.activarBotones(Boolean.valueOf(true));
        
        ComboCita(null);
        
        this.citas = this.modelo.readc();
        if (this.citas.size() > 0) {
          primero();
          mostrar(this.activa);
        }
       
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Object evento = e.getSource();
        Cita cita = new Cita();
        JFrame frame = new JFrame();
        if (this.vistacita.botonPrimero == evento){
            primero();
            mostrar(this.activa);
        }
        else if (this.vistacita.botonAnterior == evento){
            anterior();
            mostrar(this.activa);
        }
        else if (this.vistacita.botonSiguiente == evento){
            siguiente();
            mostrar(this.activa);
        }
        else if (this.vistacita.botonUltimo == evento){
            ultimo();
            mostrar(this.activa);
        }
        else if(this.vistacita.botonCreate == evento){
               
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistacita.botonFecha.requestFocus();//de esta forma el puntero de editar empieza aqui
            vaciarCampos();
            editarCampos(Boolean.valueOf(true));
            vistacita.txtId.setEditable(false);
            activarBotones(Boolean.valueOf(false));
            vistacita.botonMedico.setVisible(true);
            vistacita.botonPaciente.setVisible(true);
            VerificarCampos();
            this.opcion = "create";
            
        }else if (vistacita.botonRead == evento){
            
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistacita.txtId.requestFocus();
            vaciarCampos();
            vistacita.txtId.setEditable(true);
            activarBotones(Boolean.valueOf(false));
            
            this.opcion = "read";   
                
        }else if(vistacita.botonUpdate == evento){
            
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistacita.txtId.requestFocus();
            vaciarCampos();
            vistacita.txtId.setEditable(true);
            activarBotones(Boolean.valueOf(false));
            VerificarCampos();
            
            this.opcion = "update";  
                
        }else if(vistacita.botonDelete == evento){
            
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistacita.txtId.requestFocus();
            vaciarCampos();
            vistacita.txtId.setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "delete";  
          
        }else if (vistacita.botonCancelar == evento){
            
            vaciarCampos();
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            vistacita.botonPaciente.setVisible(false);
            vistacita.botonMedico.setVisible(false);
            primero();
           
        }else if (vistacita.botonAceptar == evento){
            menuAceptar(opcion);
            vistacita.botonPaciente.setVisible(true);
            vistacita.botonMedico.setVisible(true);
            
        }else if (vistacita.botonGuardar == evento){
             
            cita = obtener();
            modelo.update(cita);
            citas = modelo.readc();
            
            vistacita.botonGuardar.setVisible(false);
            vistacita.botonAceptar.setVisible(true);
            
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            this.activa = cita;
            
            
            
        }else if (vistacita.botonBorrar == evento){
            
            cita = obtener();
            modelo.delete(cita);
            citas = modelo.readc();
            vaciarCampos();
            vistacita.botonBorrar.setVisible(false);
            vistacita.botonAceptar.setVisible(true);
            
            anterior();
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            
           
            
        }else if (vistacita.botonMedico == evento){
            
            if (!this.vistacita.comboMedico.getSelectedItem().equals("No existe")){
               cita.setMedico((Medico) vistacita.comboMedico.getSelectedItem());
               id_medico = cita.getMedico().getId();
               nombre_medico = cita.getMedico().getNombre();
               dni_medico = cita.getMedico().getDni();
               colegiado = cita.getMedico().getNcolegiado();
               especialidad = cita.getMedico().getEspecialidad();
               edad_medico = cita.getMedico().getEdad();
               telefono_medico = cita.getMedico().getTelefono();
               datos_medico = "Id: "          + id_medico    + "\nNombre: "       + nombre_medico 
                          + "\nDni: "         + dni_medico   + "\nNº Colegiado: " + colegiado 
                          + "\nEspecialida: " + especialidad + "\nEdad: "         + edad_medico 
                          + "\nTelefono: "    + telefono_medico;
            JOptionPane.showMessageDialog(frame, datos_medico , "Datos del medico", JOptionPane.WARNING_MESSAGE); 
            }
        }else if (vistacita.botonPaciente == evento){
            
                if (!this.vistacita.comboPaciente.getSelectedItem().equals("No existe")){
                   cita.setPaciente((Paciente) vistacita.comboPaciente.getSelectedItem());
                   id_paciente = cita.getPaciente().getId();
                   nombre_paciente = cita.getPaciente().getNombre();
                   dni_paciente = cita.getPaciente().getDni();
                   nss = cita.getPaciente().getNss();
                   edad_paciente = cita.getPaciente().getEdad();
                   telefono_paciente = cita.getPaciente().getTelefono();
                   datos_paciente = "Id: "          + id_paciente    + "\nNombre: "       + nombre_paciente
                              + "\nDni: "         + dni_paciente  + "\nNº Seguridad Social: " + nss 
                              + "\nEdad: "         + edad_paciente
                              + "\nTelefono: "    + telefono_paciente;
                JOptionPane.showMessageDialog(frame, datos_paciente , "Datos del paciente", JOptionPane.WARNING_MESSAGE); 
                }
           
            }else if (vistacita.botonSalir == evento){
            
            this.vistacita.ventana.setVisible(false);
            
            }
        
    }
    public void menuAceptar(String opcion) {
        JFrame frame = new JFrame();
        
        switch(opcion){
            case "create":
                
                if(//!vistacita.txtId.getText().equals("") && 
                   !vistacita.botonFecha.getDate().equals("") && 
                   !vistacita.txtHora.getText().equals("")){
                        Cita cita = obtener();
                        modelo.create(cita);
                        citas.add(cita);
                        mostrar(cita);
                        editarCampos(Boolean.valueOf(false));
                        activarBotones(Boolean.valueOf(true));
                        this.activa = cita;

                }else JOptionPane.showMessageDialog(frame, "ERROR: Completa todos los datos", "Mensaje de Error", JOptionPane.WARNING_MESSAGE);
                
                break;
            case "read":
               if (vistacita.txtId.getText() != ("")){
                  getCita(Integer.parseInt(vistacita.txtId.getText()));
                             
                 }
               activarBotones(Boolean.valueOf(false));
               break;
            case "update":
                if (vistacita.txtId.getText() != ("")){
                  getCita(Integer.parseInt(vistacita.txtId.getText()));
                 }
                vistacita.botonGuardar.setVisible(true);
                editarCampos(Boolean.valueOf(true));
                vistacita.txtId.setEditable(false);
                activarBotones(Boolean.valueOf(false));
                vistacita.botonAceptar.setVisible(false);
                break;
            case "delete":
                if (vistacita.txtId.getText() != ("")){
                  getCita(Integer.parseInt(vistacita.txtId.getText()));
                }
                vistacita.botonBorrar.setVisible(true);
                editarCampos(Boolean.valueOf(false));
                vistacita.txtId.setEditable(false);
                activarBotones(Boolean.valueOf(false));
                vistacita.botonAceptar.setVisible(false);
                break;
           
        }
    }
       
    public void activarBotones(Boolean booleano){
        
        this.vistacita.botonCreate.setEnabled(booleano.booleanValue());
        this.vistacita.botonRead.setEnabled(booleano.booleanValue());
        this.vistacita.botonUpdate.setEnabled(booleano.booleanValue());
        this.vistacita.botonDelete.setEnabled(booleano.booleanValue());
        this.vistacita.botonCancelar.setEnabled(!booleano.booleanValue());
        this.vistacita.botonAceptar.setEnabled(!booleano.booleanValue());
        this.vistacita.botonMedico.setEnabled(!booleano.booleanValue());
        this.vistacita.botonPaciente.setEnabled(!booleano.booleanValue());
    }
    
    public void editarCampos(Boolean booleano){
        
        this.vistacita.txtId.setEditable(booleano.booleanValue());
        this.vistacita.botonFecha.setEnabled(booleano.booleanValue());
        this.vistacita.txtHora.setEditable(booleano.booleanValue());
        this.vistacita.txtObservaciones.setEditable(booleano.booleanValue());
        this.vistacita.comboPaciente.setEnabled(booleano.booleanValue());
        this.vistacita.comboMedico.setEnabled(booleano.booleanValue());
              
    }
    
     public void vaciarCampos (){
        this.vistacita.txtId.setText("");
        this.vistacita.botonFecha.setDate(null);
        this.vistacita.txtHora.setText("");
        this.vistacita.txtObservaciones.setText("");
        this.vistacita.comboMedico.setSelectedItem(null);
        this.vistacita.comboPaciente.setSelectedItem(null);
           
    }
     
     private Cita obtener() {
        Medico medico = new Medico();
        Paciente paciente = new Paciente();
        Cita cita = new Cita();
        JFrame frame = new JFrame();
       
        int id = 0;
            
           try{
                  id = Integer.parseInt(this.vistacita.txtId.getText());
                  cita.setId(id);
                }catch (NumberFormatException ex){
                  //cita.setId(0);
                }
           
            cita.setDia(this.vistacita.botonFecha.getDate());
            cita.setHora(this.vistacita.txtHora.getText());
            if(this.vistacita.txtObservaciones.getText().equals("")){
                cita.setObservaciones(" ");
            }else{
            cita.setObservaciones(this.vistacita.txtObservaciones.getText());
            }
            
            if (!this.vistacita.comboMedico.getSelectedItem().equals("No existe")){
               cita.setMedico((Medico) vistacita.comboMedico.getSelectedItem());
               
            }else JOptionPane.showMessageDialog(frame, "Selecciona un medico", "Mensaje de Error", JOptionPane.WARNING_MESSAGE); 
            if (!this.vistacita.comboPaciente.getSelectedObjects().equals("No existe")){
               cita.setPaciente((Paciente) vistacita.comboPaciente.getSelectedItem());
               
            }else JOptionPane.showMessageDialog(frame, "Selecciona un paciente", "Mensaje de Error", JOptionPane.WARNING_MESSAGE); 
            
            return cita;
    }

   
private void getCita(int id) {
                      
        Iterator iterator = modelo.readc().iterator();
        while (iterator.hasNext()) {                    
            Cita cita = (Cita) iterator.next();
            if (id == cita.getId()) {                
                   //this.vistamedico.txtId.setText(medico.getId());
                this.vistacita.botonFecha.setDate(cita.getDia());
                this.vistacita.txtHora.setText(cita.getHora());
                this.vistacita.txtObservaciones.setText(cita.getObservaciones());
                ComboCita(cita);
                this.activa = cita;
            } 
               
        }
}
private void primero() {
        
        if (this.citas != null){
            this.activa = ((Cita)this.citas.get(0));
       } 
               
    }   
    private void anterior() {
       if (this.contActiva != 0){
            this.contActiva -= 1;
            this.activa = ((Cita)this.citas.get(this.contActiva));
        }
        
    }

    private void siguiente() {
        if (this.contActiva != this.citas.size() - 1){
            this.contActiva += 1;
            this.activa = ((Cita)this.citas.get(this.contActiva));
        }
    }

    private void ultimo() {
        
        this.contActiva = (this.citas.size()-1);
        this.activa = ((Cita)this.citas.get(this.contActiva));
                 
    }
    
    private void mostrar(Cita primera){
        
        editarCampos(Boolean.valueOf(false));
        activarBotones(Boolean.valueOf(true));
        
        if (primera == null) {
           
          return;
        }
        
        ComboCita(primera);
        
        if (primera == null) {
          return;
        }
            
            this.vistacita.txtId.setText(String.valueOf(primera.getId()));
            this.vistacita.botonFecha.setDate(primera.getDia());
            this.vistacita.txtHora.setText(primera.getHora());
            this.vistacita.txtObservaciones.setText(primera.getObservaciones());
           
    }
   
    private void ComboCita(Cita cita){
        
        
        ArrayList medicos = this.modelo.readm();
        ArrayList pacientes = this.modelo.readp();
          
        if (medicos != null){
            this.vistacita.comboMedico.removeAllItems();
            this.vistacita.comboMedico.addItem("No existe");
            for (int i = 0; i < medicos.size(); i++){
                this.vistacita.comboMedico.addItem(medicos.get(i));
                
                Medico medico = (Medico)medicos.get(i);
                
                    if ((cita != null) && (cita.getMedico().getId() != 0) && 
                        (medico.getId() == (cita.getMedico()).getId())){
                        this.vistacita.comboMedico.setSelectedItem(medicos.get(i));
                    }
                        
            }
        }
        if (pacientes != null){
            this.vistacita.comboPaciente.removeAllItems();
            this.vistacita.comboPaciente.addItem("No existe");
            for (int i = 0; i < pacientes.size(); i++){
                this.vistacita.comboPaciente.addItem(pacientes.get(i));
                
                Paciente paciente = (Paciente)pacientes.get(i);
                
                    if ((cita != null) && (cita.getPaciente().getId() != 0) && 
                        (paciente.getId() == (cita.getPaciente()).getId())){
                        this.vistacita.comboPaciente.setSelectedItem(pacientes.get(i));
                    }
                        
            }
        }
        
    }
     
    private Medico getMedico(int id) {
        
        Medico medico = null;  
        
        Iterator iterator = modelo.readm().iterator();
        while (iterator.hasNext()) {                    
            Medico me = (Medico) iterator.next();
            if (id==(me.getId())) {                
                medico = me;
            }else medico = null;
        } 
        return medico;                                 
    }
    
    
     private Paciente getPaciente(int id) {
         
        Paciente paciente = null;
        
        Iterator iterator = modelo.readp().iterator();
        while (iterator.hasNext()) {                    
            Paciente pa = (Paciente) iterator.next();
            if (id == (pa.getId())) {                
                paciente = pa;
            }else paciente = null;
        } 
        return paciente;                                 
    }
     
     private void VerificarCampos(){
        this.vistacita.txtHora.setInputVerifier(new VerificarCampos("hora"));
       
    } 
  
}