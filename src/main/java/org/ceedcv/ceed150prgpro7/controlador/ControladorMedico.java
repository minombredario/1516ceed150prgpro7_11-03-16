
package org.ceedcv.ceed150prgpro7.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.ceedcv.ceed150prgpro7.modelo.*;
import org.ceedcv.ceed150prgpro7.vista.*;


/**
 * Fichero: ControladorMedico.java
 *
 * @author Darío Navarro Andrés minombredario@gmail.com
 * 
 */
public class ControladorMedico implements ActionListener{

   
    VistaGraficaMedico vistamedico;
    IModelo modelo;
    String opcion;
    ArrayList medicos;
    Medico activo;
    int contActivo;
    
    
    public ControladorMedico(VistaGraficaMedico vistamedico, IModelo modelo) throws IOException {
        this.vistamedico = vistamedico;
        this.modelo = modelo;
        
        this.vistamedico.CrearMenuMedico();
        this.vistamedico.ventana.setVisible(true);
        this.vistamedico.botonCreate.addActionListener(this);
        this.vistamedico.botonRead.addActionListener(this);
        this.vistamedico.botonUpdate.addActionListener(this);
        this.vistamedico.botonDelete.addActionListener(this);
        this.vistamedico.botonCancelar.addActionListener(this);
        this.vistamedico.botonAceptar.addActionListener(this);
        this.vistamedico.botonSalir.addActionListener(this);
        this.vistamedico.botonGuardar.addActionListener(this);
        this.vistamedico.botonBorrar.addActionListener(this);
        this.vistamedico.botonPrimero.addActionListener(this);
        this.vistamedico.botonUltimo.addActionListener(this);
        this.vistamedico.botonSiguiente.addActionListener(this);
        this.vistamedico.botonAnterior.addActionListener(this);
        
                
        editarCampos(Boolean.valueOf(false));
        activarBotones(Boolean.valueOf(true));
       
        
        this.medicos = this.modelo.readm();
        if (this.medicos.size() > 0) {
          primero();
          mostrar(this.activo);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object evento = e.getSource();
        if (this.vistamedico.botonPrimero == evento){
            primero();
            mostrar(this.activo);
        }
        else if (this.vistamedico.botonAnterior == evento){
            anterior();
            mostrar(this.activo);
        }
        else if (this.vistamedico.botonSiguiente == evento){
            siguiente();
            mostrar(this.activo);
        }
        else if (this.vistamedico.botonUltimo == evento){
            ultimo();
            mostrar(this.activo);
        }
        else if(this.vistamedico.botonCreate == evento){
            Medico medico = new Medico();     
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistamedico.txtNombre.requestFocus();//de esta forma el puntero de editar empieza aqui
            vaciarCampos();
            editarCampos(Boolean.valueOf(true));
            vistamedico.txtId.setEditable(false);
            activarBotones(Boolean.valueOf(false));
            VerificarCampos();
            
            
            this.opcion = "create";
        }else if (vistamedico.botonRead == evento){
            
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistamedico.txtId.requestFocus();
            vaciarCampos();
            vistamedico.txtId.setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "read";   
                
        }else if(vistamedico.botonUpdate == evento){
            
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistamedico.txtId.requestFocus();
            vaciarCampos();
            vistamedico.txtId.setEditable(true);
            activarBotones(Boolean.valueOf(false));
            VerificarCampos();
            
            this.opcion = "update";  
                
        }else if(vistamedico.botonDelete == evento){
            
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vistamedico.txtId.requestFocus();
            vaciarCampos();
            vistamedico.txtId.setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "delete";  
          
        }else if (vistamedico.botonCancelar == evento){
            
            vaciarCampos();
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            primero();
           
        }else if (vistamedico.botonAceptar == evento){
            menuAceptar(opcion);
            
        }else if (vistamedico.botonGuardar == evento){
            
            Medico medico = new Medico();
            medico = obtener();
            //medico.setId(vistamedico.txtId.getText());
            modelo.update(medico);
            medicos = modelo.readm();
            vistamedico.botonGuardar.setVisible(false);
            vistamedico.botonAceptar.setVisible(true);
            
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            this.activo = medico;
        }else if (vistamedico.botonBorrar == evento){
            
            Medico medico = new Medico();
            medico = obtener();
            modelo.delete(medico);
            medicos = modelo.readm();
            vaciarCampos();
            vistamedico.botonBorrar.setVisible(false);
            vistamedico.botonAceptar.setVisible(true);
            anterior();
            
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            
        }else if (vistamedico.botonSalir == evento){
            
            this.vistamedico.ventana.setVisible(false);
            
        }
    }
    public void menuAceptar(String opcion) {
        JFrame frame = new JFrame();
        
        switch(opcion){
            case "create":
                
                if(//!vistamedico.txtId.getText().equals("") && 
                   !vistamedico.txtNombre.getText().equals("") && 
                   !vistamedico.txtDni.getText().equals("") && 
                   !vistamedico.txtNcolegiado.getText().equals("") && 
                   !vistamedico.txtEspecialidad.getText().equals("") && 
                   !vistamedico.txtEdad.getText().equals("") && 
                   !vistamedico.txtTelefono.getText().equals("")){
                        Medico medico = obtener();
                        modelo.create(medico);
                        medicos.add(medico);
                        mostrar(medico);
                        editarCampos(Boolean.valueOf(false));
                        activarBotones(Boolean.valueOf(true));
                        this.activo = medico;

                }else JOptionPane.showMessageDialog(frame, "ERROR: Completa todos los datos", "Mensaje de Error", JOptionPane.WARNING_MESSAGE);
                
                break;
            case "read":
               if (vistamedico.txtId.getText() != ("")){
                  getMedico(Integer.parseInt(vistamedico.txtId.getText()));
                             
                 }
               activarBotones(Boolean.valueOf(false));
               break;
            case "update":
                if (!vistamedico.txtId.getText().equals("")){
                  getMedico(Integer.parseInt(vistamedico.txtId.getText()));
                 }
                vistamedico.botonGuardar.setVisible(true);
                editarCampos(Boolean.valueOf(true));
                vistamedico.txtId.setEditable(false);
                activarBotones(Boolean.valueOf(false));
                vistamedico.botonAceptar.setVisible(false);
                 break;
            case "delete":
                if (!vistamedico.txtId.getText().equals("")){
                  getMedico(Integer.parseInt(vistamedico.txtId.getText()));
                }
                vistamedico.botonBorrar.setVisible(true);
                editarCampos(Boolean.valueOf(false));
                vistamedico.txtId.setEditable(false);
                activarBotones(Boolean.valueOf(false));
                vistamedico.botonAceptar.setVisible(false);
                break;
           
        }
    }
       
    public void activarBotones(Boolean booleano){
        
        this.vistamedico.botonCreate.setEnabled(booleano.booleanValue());
        this.vistamedico.botonRead.setEnabled(booleano.booleanValue());
        this.vistamedico.botonUpdate.setEnabled(booleano.booleanValue());
        this.vistamedico.botonDelete.setEnabled(booleano.booleanValue());
        this.vistamedico.botonCancelar.setEnabled(!booleano.booleanValue());
        this.vistamedico.botonAceptar.setEnabled(!booleano.booleanValue());
    }
    
    public void editarCampos(Boolean booleano){
        
        this.vistamedico.txtId.setEditable(booleano.booleanValue());
        this.vistamedico.txtNombre.setEditable(booleano.booleanValue());
        this.vistamedico.txtDni.setEditable(booleano.booleanValue());
        this.vistamedico.txtNcolegiado.setEditable(booleano.booleanValue());
        this.vistamedico.txtEspecialidad.setEditable(booleano.booleanValue());
        this.vistamedico.txtEdad.setEditable(booleano.booleanValue());
        this.vistamedico.txtTelefono.setEditable(booleano.booleanValue());
        this.vistamedico.txtObservaciones.setEditable(booleano.booleanValue());
        
              
    }
    
     public void vaciarCampos (){
        this.vistamedico.txtId.setText("");
        this.vistamedico.txtNombre.setText("");
        this.vistamedico.txtDni.setText("");
        this.vistamedico.txtNcolegiado.setText("");
        this.vistamedico.txtEspecialidad.setText("");
        this.vistamedico.txtEdad.setText("");
        this.vistamedico.txtTelefono.setText("");
        this.vistamedico.txtObservaciones.setText("");
           
    }
     
    private Medico obtener() {
       
        Medico medico = new Medico();
        int edad, telefono, id = 0;
            
           try{
                  id = Integer.parseInt(this.vistamedico.txtId.getText());
                  medico.setId(id);
                }catch (NumberFormatException ex){
                  //medico.setId(0);
                }
           
            medico.setNombre(this.vistamedico.txtNombre.getText());
            medico.setDni(this.vistamedico.txtDni.getText());
            medico.setNcolegiado(this.vistamedico.txtNcolegiado.getText());
            medico.setEspecialidad(this.vistamedico.txtEspecialidad.getText());
            
            if(this.vistamedico.txtObservaciones.getText().equals("")){
                medico.setObservaciones(" ");
                }else{
                     medico.setObservaciones(this.vistamedico.txtObservaciones.getText());
                }  

                try{
                  edad = Integer.parseInt(this.vistamedico.txtEdad.getText());
                  medico.setEdad(edad);
                }catch (NumberFormatException ex){
                  medico.setEdad(0);
                }

                 try{
                  telefono =Integer.parseInt(this.vistamedico.txtTelefono.getText());
                  medico.setTelefono(telefono);
                }catch (NumberFormatException ex){
                  medico.setTelefono(000000000);
                }

            return medico;

    }

   
private void getMedico(int id) {
                      
        Iterator iterator = modelo.readm().iterator();
        while (iterator.hasNext()) {                    
            Medico medico = (Medico) iterator.next();
            if (id == medico.getId()) {                
                   //this.vistamedico.txtId.setText(medico.getId());
                this.vistamedico.txtNombre.setText(medico.getNombre());
                this.vistamedico.txtDni.setText(medico.getDni());
                this.vistamedico.txtNcolegiado.setText(medico.getNcolegiado());
                this.vistamedico.txtEspecialidad.setText(medico.getEspecialidad());
                this.vistamedico.txtEdad.setText(String.valueOf(medico.getEdad()));
                this.vistamedico.txtTelefono.setText(String.valueOf(medico.getTelefono()));
                this.vistamedico.txtObservaciones.setText(medico.getObservaciones());
                this.activo = medico;
            } 
               
        }
}
    private void esperarXsegundos(int segundos) {
	try {
            Thread.sleep(segundos * 1000);
	} catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
	}
    }

    private void primero() {
        
        if (this.medicos != null){
            this.activo = ((Medico)this.medicos.get(0));
       } 
               
    }   
    private void anterior() {
       if (this.contActivo != 0){
            this.contActivo -= 1;
            this.activo = ((Medico)this.medicos.get(this.contActivo));
        }
        
    }

    private void siguiente() {
        if (this.contActivo != this.medicos.size() - 1){
            this.contActivo += 1;
            this.activo = ((Medico)this.medicos.get(this.contActivo));
        }
    }

    private void ultimo() {
        
        this.contActivo = (this.medicos.size()-1);
        this.activo = ((Medico)this.medicos.get(this.contActivo));
                 
    }
    
    private void mostrar(Medico primero){
        
        editarCampos(Boolean.valueOf(false));
        activarBotones(Boolean.valueOf(true));
        if (primero == null) {
          return;
        } else {
            
            this.vistamedico.txtId.setText(String.valueOf(primero.getId()));
            this.vistamedico.txtNombre.setText(primero.getNombre());
            this.vistamedico.txtDni.setText(primero.getDni());
            this.vistamedico.txtNcolegiado.setText(primero.getNcolegiado());
            this.vistamedico.txtEspecialidad.setText(primero.getEspecialidad());
            this.vistamedico.txtEdad.setText(String.valueOf(primero.getEdad()));
            this.vistamedico.txtTelefono.setText(String.valueOf(primero.getTelefono()));
            this.vistamedico.txtObservaciones.setText(primero.getObservaciones());
                    
        }
    }
    
    private void VerificarCampos(){
        this.vistamedico.txtEdad.setInputVerifier(new VerificarCampos("txtEdad"));
        this.vistamedico.txtDni.setInputVerifier(new VerificarCampos("txtDni"));
        this.vistamedico.txtNcolegiado.setInputVerifier(new VerificarCampos("txtNcolegiado"));
        this.vistamedico.txtTelefono.setInputVerifier(new VerificarCampos("txtTelefono"));
        this.vistamedico.txtEspecialidad.setInputVerifier(new VerificarCampos("txtEspecialidad"));
        this.vistamedico.txtNombre.setInputVerifier(new VerificarCampos("txtNombre"));
    } 
    
}
