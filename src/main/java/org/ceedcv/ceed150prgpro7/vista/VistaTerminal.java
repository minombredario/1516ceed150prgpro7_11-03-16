package org.ceedcv.ceed150prgpro7.vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Fichero: VistaTerminal.java
 * 
 * @author Dario Navarro Andres minombredario@gmail.com
 *
 */
public class VistaTerminal {
   
    public int pedirInt() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        String linea;
        int numero = 0;
        try{
             linea = buffer.readLine();
            numero = Integer.parseInt(linea);
        }catch (Exception e){
            System.err.println("Introduce un valor númerico");
            
        }
        return numero;

    }

    public String pedirString() {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        String linea = null;

        try {
            linea = buffer.readLine();
        } catch (Exception e) {
            System.err.println("Introduce texto");
        }
        return linea;
    }

    public void Esperar() throws IOException {
        Scanner continuar = new Scanner(System.in);
        System.out.println("Pulsa cualquier tecla para continuar");
        continuar.nextLine();
    }
           
    //Función para mostrar un texto por pantalla
    public void mostrarTexto(String texto) {
        System.out.print(texto);
    }
    
    //Función para validar un telefono 
    public boolean getTelefono (String telefono){
        final String patronTelefono = "[^[9|8|7|6]\\d{8}$";
    Pattern patron = Pattern.compile(patronTelefono);
    Matcher compara = patron.matcher(telefono);
    return compara.matches();
    }
   
    //Función para leer una letra por teclado y pasarla a minúscula
    public static char leerLetra() {
        String linea;
        char opcion;
        Scanner sc = new Scanner (System.in);
        linea = sc.next().toLowerCase();
        opcion = linea.charAt(0);
        return opcion;
    }
   
    public String getFecha() {
          
        mostrarTexto("\nIntroduzca la fecha con formato dd/mm/yyyy");
        mostrarTexto("\nDia: ");
        String dia = pedirString();
         //vuelve una fecha en string a tipo date 
         //pero el formato debe ser MM/dd/yyyy 
         Date date = new Date(dia); 
         //esto vuelve un tipo date a string 
         SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
         String fecha = sdf.format(date);//la volvemos string 
         return fecha; 

    }
      
      public String getHora(DateFormat hora) {
       Date date = new Date();
       String convertido;
       hora = new SimpleDateFormat("HH:mm");
       convertido = hora.format(date);
      
       return convertido;
    }
      
}

