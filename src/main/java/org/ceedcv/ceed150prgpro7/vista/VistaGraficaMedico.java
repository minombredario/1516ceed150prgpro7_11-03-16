/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed150prgpro7.vista;

import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import javax.swing.*;


/**

* Fichero: VistaGraficaMedico.java

* @author Darío Navarro Andrés minombredario@gmail.com

*

*/
public class VistaGraficaMedico {
    private JLabel etiId,etiNombre, etiEdad, etiTelefono, etiObservaciones, etiNcolegiado, etiEspecialidad, etiDni, etiImagen, etiMenu;
    public JTextField txtId, txtNombre, txtEdad, txtTelefono, txtNcolegiado, txtEspecialidad, txtDni;
    public JButton botonCreate, botonUpdate, botonRead, botonDelete, botonSalir, botonAceptar, botonCancelar, botonGuardar, botonBorrar, botonPrimero, botonUltimo, botonSiguiente, botonAnterior;
    public JTextArea txtObservaciones;
    public JFrame ventana;
    private JPanel panel;
    private ImageIcon imagen;

    public void CrearMenuMedico(){
        
        panel = new JPanel();
        panel.setLayout(null);
        String img = "src/main/java/Imagenes/doctorlogo.png";
        
        etiMenu = new JLabel("MENU MEDICO");
        etiMenu.setBounds(230, 5, 220, 35);
        etiMenu.setFont(new Font("Helvetica", Font.BOLD, 30));
        etiMenu.setForeground(Color.LIGHT_GRAY);
        //etiMenu.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
        panel.add(etiMenu);
        
        etiId = new JLabel("Id: ");
        etiId.setBounds(10, 20, 80, 20);
        etiNombre = new JLabel("Nombre: ");
        etiNombre.setBounds(10, 50, 80, 20);
        etiEdad = new JLabel("Edad: ");
        etiEdad.setBounds(10, 80, 80, 20);
        etiTelefono = new JLabel("Telefono: ");
        etiTelefono.setBounds(10, 110, 80, 20);
        etiObservaciones = new JLabel("Observaciones: ");
        etiObservaciones.setBounds(10, 140, 100, 20);
        
        etiDni = new JLabel ("Dni: ");
        etiDni.setBounds(200, 50, 80, 20);
        etiNcolegiado = new JLabel("Número Colegiado: ");
        etiNcolegiado.setBounds(200, 80, 120, 20);
        etiEspecialidad = new JLabel("Especialidad: ");
        etiEspecialidad.setBounds(200, 110, 80, 20);
        
        imagen = new ImageIcon (img);
        etiImagen = new JLabel(imagen);
        etiImagen.setBounds(385, 22, 323, 380);
        
        
        
        txtId = new JTextField();
        txtId.setBounds(75, 20, 100, 20);
        txtNombre = new JTextField();
        txtNombre.setBounds(75, 50, 100, 20);
        txtEdad = new JTextField();
        txtEdad.setBounds(75, 80, 100, 20);
        txtTelefono = new JTextField();
        txtTelefono.setBounds(75, 110, 100, 20);
        txtDni = new JTextField();
        txtDni.setBounds(320, 50, 100, 20);
        txtNcolegiado = new JTextField();
        txtNcolegiado.setBounds(320, 80, 100, 20);
        txtEspecialidad = new JTextField();
        txtEspecialidad.setBounds(320, 110, 100, 20);
       
        
        txtObservaciones = new JTextArea();
        txtObservaciones.setBounds(10, 160, 390, 150);
        
        botonCreate = new JButton("CREATE");
        botonCreate.setBounds(10, 350, 90, 20);
        botonRead = new JButton("READ");
        botonRead.setBounds(110, 350, 90, 20);
        botonUpdate = new JButton("UPDATE");
        botonUpdate.setBounds(210, 350, 90, 20);
        botonDelete = new JButton("DELETE");
        botonDelete.setBounds(310, 350, 90, 20);
        botonAceptar = new JButton("ACEPTAR");
        botonAceptar.setBounds(440, 350, 100, 20);
        botonCancelar = new JButton("CANCELAR");
        botonCancelar.setBounds(560, 350, 100, 20);
        botonSalir = new JButton("SALIR");
        botonSalir.setBounds(560, 10, 100, 20);
        botonGuardar = new JButton("GUARDAR");
        botonGuardar.setBounds(440, 350, 100, 20);
        botonGuardar.setVisible(false);
        botonBorrar = new JButton("BORRAR");
        botonBorrar.setBounds(440, 350, 100, 20);
        botonBorrar.setVisible(false);
        
        botonPrimero = new JButton("<<");
        botonPrimero.setBounds(70, 320, 50, 20);
        botonAnterior =new JButton("<");
        botonAnterior.setBounds(140, 320, 50, 20);
        botonSiguiente = new JButton(">");
        botonSiguiente.setBounds(210, 320, 50, 20);
        botonUltimo = new JButton (">>");
        botonUltimo.setBounds(280, 320, 50, 20);
        
       
        panel.add(etiId);
        panel.add(etiNombre);
        panel.add(etiDni);
        panel.add(etiEdad);
        panel.add(etiTelefono);
        panel.add(etiObservaciones);
        panel.add(txtId);
        panel.add(txtNombre);
        panel.add(txtEdad);
        panel.add(txtTelefono);
        panel.add(txtObservaciones);
        panel.add(txtDni);
        
        panel.add(botonCreate);
        panel.add(botonUpdate);
        panel.add(botonRead);
        panel.add(botonDelete);
        panel.add(botonAceptar);
        panel.add(botonSalir);
        panel.add(botonCancelar);
        panel.add(botonGuardar);
        panel.add(botonBorrar);
        
        panel.add(botonPrimero);
        panel.add(botonSiguiente);
        panel.add(botonAnterior);
        panel.add(botonUltimo);
        
                         
        panel.add(etiNcolegiado);
        panel.add(etiEspecialidad);
        panel.add(txtNcolegiado);
        panel.add(txtEspecialidad);
        
        panel.add(etiImagen);
        
        
        
        
        ventana = new JFrame();
        ventana.setTitle("Consulta Medica Privada");
        ventana.setSize(700,420);
        //este metodo devuelve el tamaño de la pantalla
            Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        //obtenemos el tamaño de la ventana
            Dimension window = ventana.getSize();
        //para centrar la ventana lo hacemos con el siguiente calculo
            ventana.setLocation((pantalla.width-window.width)/2, (pantalla.height-window.height)/2);
            
        ventana.setDefaultCloseOperation(ventana.ICONIFIED);//IGUAL QUE HIDE_ON_CLOSE CIERRA LA VENTANA PERO NO EL PROGRAMA
        Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
        teclas.add(AWTKeyStroke.getAWTKeyStroke(
                KeyEvent.VK_ENTER, 0));
        teclas.add(AWTKeyStroke.getAWTKeyStroke(
                KeyEvent.VK_TAB, 0));
        
        // Se pasa el conjunto de teclas al panel principal 
        ventana.getContentPane().setFocusTraversalKeys(
                KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, 
                teclas);
        ventana.add(panel);
        
        //ventana.setVisible(true);
        
    }
    
   /* public VistaGraficaMedico(){
        CrearMenuMedico();
        
    }
    
    public static void main(String[] args) {
        VistaGraficaMedico principal = new VistaGraficaMedico();
    }*/

   
}
