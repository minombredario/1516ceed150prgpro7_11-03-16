/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed150prgpro7.vista;

import com.toedter.calendar.JDateChooser;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.swing.*;
import org.ceedcv.ceed150prgpro7.modelo.Medico;

/**
* Fichero: VistaGraficaCita.java
* @author Darío Navarro Andrés minombredario@gmail.com
*
*/
public class VistaGraficaCita {
    
    private JLabel etiId, etiFecha, etiHora, etiObservaciones, etiMenu, etiMedico, 
            etiPaciente, etiImagen;
    public JTextField txtId, /*txtFecha,*/ txtHora;
    public  JButton botonCreate, botonUpdate, botonRead, botonDelete, botonSalir,
            botonGuardar,botonCancelar, botonAceptar, botonBorrar, botonPrimero, 
            botonUltimo, botonAnterior, botonSiguiente, botonPaciente, botonMedico;
    public JTextArea txtObservaciones;
    public JFrame ventana;
    private JPanel panel;
    private ImageIcon imagen;
    public JComboBox comboPaciente, comboMedico;
    public JDateChooser botonFecha;
    
    
    public void CrearMenuCita(){
       
        panel = new JPanel();
        panel.setLayout(null);
        String img = "src/main/java/Imagenes/cita.png";
        
        etiMenu = new JLabel("MENU CITA");
        etiMenu.setBounds(270, 5, 170, 35);
        etiMenu.setFont(new Font("Helvetica", Font.BOLD, 30));
        etiMenu.setForeground(Color.LIGHT_GRAY);
        //etiMenu.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
        panel.add(etiMenu);
        
        etiId = new JLabel("Id: ");
        etiId.setBounds(10, 20, 80, 20);
        etiFecha = new JLabel("Fecha: ");
        etiFecha.setBounds(10, 50, 80, 20);
        etiHora = new JLabel("Hora: ");
        etiHora.setBounds(10, 80, 80, 20);
        etiObservaciones = new JLabel("Observaciones: ");
        etiObservaciones.setBounds(10, 140, 100, 20);
        
        etiMedico = new JLabel ("Medico: ");
        etiMedico.setBounds(200, 50, 80, 20);
        etiPaciente = new JLabel("Paciente: ");
        etiPaciente.setBounds(200, 80, 120, 20);
                       
        imagen = new ImageIcon (img);
        etiImagen = new JLabel(imagen);
        etiImagen.setBounds(420, 5, 250, 350);
        
        
        
        txtId = new JTextField();
        txtId.setBounds(75, 20, 100, 20);
        //txtFecha = new JTextField();
        //txtFecha.setBounds(75, 50, 100, 20);
        txtHora = new JTextField();
        txtHora.setBounds(75, 80, 100, 20);
        comboPaciente = new JComboBox();
        comboPaciente.setBounds(270, 80, 100, 20);
        comboMedico = new JComboBox();
        comboMedico.setBounds(270, 50, 100, 20);
       
               
        
        txtObservaciones = new JTextArea();
        txtObservaciones.setBounds(10, 160, 390, 150);
        
        botonCreate = new JButton("CREATE");
        botonCreate.setBounds(10, 350, 90, 20);
        botonRead = new JButton("READ");
        botonRead.setBounds(110, 350, 90, 20);
        botonUpdate = new JButton("UPDATE");
        botonUpdate.setBounds(210, 350, 90, 20);
        botonDelete = new JButton("DELETE");
        botonDelete.setBounds(310, 350, 90, 20);
        botonAceptar = new JButton("ACEPTAR");
        botonAceptar.setBounds(440, 350, 100, 20);
        botonCancelar = new JButton("CANCELAR");
        botonCancelar.setBounds(560, 350, 100, 20);
        botonSalir = new JButton("SALIR");
        botonSalir.setBounds(560, 10, 100, 20);
        botonGuardar = new JButton("GUARDAR");
        botonGuardar.setBounds(440, 350, 100, 20);
        botonGuardar.setVisible(false);
        botonBorrar = new JButton("BORRAR");
        botonBorrar.setBounds(440, 350, 100, 20);
        botonBorrar.setVisible(false);
        botonFecha = new JDateChooser();
        botonFecha.setBounds(75, 50, 100, 20);
        
        botonPrimero = new JButton("<<");
        botonPrimero.setBounds(70, 320, 50, 20);
        botonAnterior =new JButton("<");
        botonAnterior.setBounds(140, 320, 50, 20);
        botonSiguiente = new JButton(">");
        botonSiguiente.setBounds(210, 320, 50, 20);
        botonUltimo = new JButton (">>");
        botonUltimo.setBounds(280, 320, 50, 20);
        
        botonPaciente = new JButton("Ver");
        botonPaciente.setBounds(380, 80, 60, 20);
        botonPaciente.setVisible(false);
        botonMedico = new JButton("Ver");
        botonMedico.setBounds(380, 50, 60, 20);
        botonMedico.setVisible(false);
        
        panel.add(etiId);
        panel.add(etiFecha);
        panel.add(etiHora);
        panel.add(etiPaciente);
        panel.add(etiMedico);
        panel.add(etiObservaciones);
        panel.add(txtId);
        //panel.add(txtFecha);
        panel.add(txtHora);
        panel.add(comboPaciente);
        panel.add(comboMedico);
        
        
        panel.add(txtObservaciones);
        
        
        panel.add(botonCreate);
        panel.add(botonUpdate);
        panel.add(botonRead);
        panel.add(botonDelete);
        panel.add(botonAceptar);
        panel.add(botonSalir);
        panel.add(botonCancelar);
        panel.add(botonGuardar);
        panel.add(botonBorrar);
        
        panel.add(botonPrimero);
        panel.add(botonSiguiente);
        panel.add(botonAnterior);
        panel.add(botonUltimo);
       
        panel.add(botonPaciente);
        panel.add(botonMedico);
        
        panel.add(botonFecha);
        
        panel.add(etiImagen);
        
        
        ventana = new JFrame();
        ventana.setTitle("Consulta Medica Privada");
        ventana.setSize(700,420);
        //este metodo devuelve el tamaño de la pantalla
            Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        //obtenemos el tamaño de la ventana
            Dimension window = ventana.getSize();
        //para centrar la ventana lo hacemos con el siguiente calculo
            ventana.setLocation((pantalla.width-window.width)/2, (pantalla.height-window.height)/2);
            
        ventana.setDefaultCloseOperation(ventana.ICONIFIED);
        Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
        teclas.add(AWTKeyStroke.getAWTKeyStroke(
                KeyEvent.VK_ENTER, 0));
        teclas.add(AWTKeyStroke.getAWTKeyStroke(
                KeyEvent.VK_TAB, 0));
        
        // Se pasa el conjunto de teclas al panel principal 
        ventana.getContentPane().setFocusTraversalKeys(
                KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, 
                teclas);
        ventana.add(panel);
        
        //ventana.setVisible(true);
    }
    
    /*public VistaGraficaCita(){
        CrearMenuCita();
        
    }
    
    public static void main(String[] args) {
        VistaGraficaCita principal = new VistaGraficaCita();
    }*/
    
    
}
