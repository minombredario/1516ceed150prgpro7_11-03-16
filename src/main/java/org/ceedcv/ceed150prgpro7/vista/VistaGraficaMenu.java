/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro7.vista;


import java.awt.*;
import javafx.scene.layout.Border;
import javax.swing.*;
import static javax.swing.text.StyleConstants.Foreground;
    
public class VistaGraficaMenu {
    
    public JButton botonPaciente, botonMedico, botonCita, botonAcerca, botonDocuemtacion, botonSalir;
    private JFrame ventana;
    private JPanel panel;
    private ImageIcon imagen;
    private JLabel etiImagen, etiMenu;
   
    public void CrearMenuPrincipal(){
        
        String img = "src/main/java/Imagenes/consulta.png";
        Font font = new Font ("helvetica" , Font.BOLD, 25);
        panel = new JPanel();
        panel.setLayout(null);
       
        /*etiMenu = new JLabel("MENU PRINCIPAL");
        etiMenu.setBounds(140, 5, 440, 60);
        etiMenu.setFont(new Font("Helvetica", Font.BOLD, 50));
        etiMenu.setForeground(Color.LIGHT_GRAY);
        etiMenu.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
        panel.add(etiMenu);*/
        
               
        botonPaciente = new JButton("PACIENTE");
        botonPaciente.setBounds(10, 250, 180, 60);
        botonPaciente.setFont(font);
        botonMedico = new JButton("MEDICO");
        botonMedico.setBounds(250, 250, 180, 60);
        botonMedico.setFont(font);
        botonCita = new JButton("CITA");
        botonCita.setBounds(490, 250, 180, 60);
        botonCita.setFont(font);
        botonAcerca = new JButton("ACERCA");
        botonAcerca.setBounds(10, 330, 120, 20);
        botonDocuemtacion = new JButton("DOCUMENTACIÓN");
        botonDocuemtacion.setBounds(140, 330, 150, 20);
        botonSalir = new JButton("SALIR");
        botonSalir.setBounds(560, 330, 120, 20);
        
        imagen = new ImageIcon (img);
        etiImagen = new JLabel(imagen);
        etiImagen.setBounds(125, 2, 350, 249);
                
        panel.add(botonPaciente);
        panel.add(botonMedico);
        panel.add(botonCita);
        panel.add(botonAcerca);
        panel.add(botonDocuemtacion);
        panel.add(botonSalir);
        
        panel.add(etiImagen);
        
        ventana = new JFrame();
        ventana.setTitle("Consulta Medica Privada");
        
        //ventana.setLocationRelativeTo(null);
            ventana.setSize(700,400);
        //este metodo devuelve el tamaño de la pantalla
            Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        //obtenemos el tamaño de la ventana
            Dimension window = ventana.getSize();
        //para centrar la ventana lo hacemos con el siguiente calculo
            ventana.setLocation((pantalla.width-window.width)/2, (pantalla.height-window.height)/2);
        
       
        
        ventana.setDefaultCloseOperation(ventana.EXIT_ON_CLOSE);
        ventana.add(panel);
        
        ventana.setVisible(true);
       
    }
    
    /*public VistaGraficaMenu(){
        CrearMenuPrincipal();       
    }
    
    public static void main(String[] args) {
        VistaGraficaMenu principal = new VistaGraficaMenu();
    }*/
    
    
}