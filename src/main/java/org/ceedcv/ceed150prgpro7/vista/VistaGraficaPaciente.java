/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed150prgpro7.vista;

import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.accessibility.AccessibleContext;
import javax.swing.*;
import org.ceedcv.ceed150prgpro7.modelo.Paciente;
import org.ceedcv.ceed150prgpro7.modelo.Persona;

/**

* Fichero: VistaGraficaPaciente.java

* @author Darío Navarro Andrés minombredario@gmail.com

*

*/
public class VistaGraficaPaciente {
    
    private JLabel etiId,etiNombre, etiEdad, etiTelefono, etiObservaciones, etiNss, etiDni, etiImagen, etiMenu;
    public JTextField txtId, txtNombre, txtEdad, txtTelefono, txtNss, txtDni/*, txtObservaciones*/;
    public JButton botonCreate, botonUpdate, botonRead, botonDelete, botonSalir, botonAceptar, botonCancelar, botonGuardar, botonBorrar, botonPrimero, botonUltimo, botonSiguiente, botonAnterior;
    public JTextArea txtObservaciones;
    public JFrame ventana;
    private JPanel panel;
    private ImageIcon imagen;
    public AccessibleContext acctxtObservaciones;
    public void CrearMenuPaciente(){
        
        
        panel = new JPanel();
        panel.setLayout(null);
        String img = "src/main/java/Imagenes/familia.png";
        
        etiMenu = new JLabel("MENU PACIENTE");
        etiMenu.setBounds(220, 5, 250, 35);
        etiMenu.setFont(new Font("Helvetica", Font.BOLD, 30));
        etiMenu.setForeground(Color.LIGHT_GRAY);
        //etiMenu.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
        panel.add(etiMenu);
        
        etiId = new JLabel("Id: ");
        etiId.setBounds(10, 20, 80, 20);
        etiNombre = new JLabel("Nombre: ");
        etiNombre.setBounds(10, 50, 80, 20);
        etiEdad = new JLabel("Edad: ");
        etiEdad.setBounds(10, 80, 80, 20);
        etiTelefono = new JLabel("Telefono: ");
        etiTelefono.setBounds(10, 110, 80, 20);
        etiObservaciones = new JLabel("Observaciones: ");
        etiObservaciones.setBounds(10, 140, 100, 20);
        
        etiDni = new JLabel ("Dni: ");
        etiDni.setBounds(200, 50, 80, 20);
        etiNss = new JLabel("Nº Seguridad Social: ");
        etiNss.setBounds(200, 80, 120, 20);
                
        imagen = new ImageIcon (img);
        etiImagen = new JLabel(imagen);
        etiImagen.setBounds(420, 5, 250, 350);
        
        
        
        txtId = new JTextField();
        txtId.setBounds(75, 20, 100, 20);
        txtNombre = new JTextField();
        txtNombre.setBounds(75, 50, 100, 20);
        txtEdad = new JTextField();
        txtEdad.setBounds(75, 80, 100, 20);
        txtTelefono = new JTextField();
        txtTelefono.setBounds(75, 110, 100, 20);
        txtDni = new JTextField();
        txtDni.setBounds(320, 50, 100, 20);
        txtNss = new JTextField();
        txtNss.setBounds(320, 80, 100, 20);
               
        
        txtObservaciones = new JTextArea();
        txtObservaciones.setBounds(10, 160, 390, 150);
        // Para que haga el salto de línea en cualquier parte de la palabra: 
        txtObservaciones.setLineWrap (true);
        // Para que haga el salto de línea buscando espacios entre las palabras 
        txtObservaciones.setWrapStyleWord(true);
        
       
        
        
        botonCreate = new JButton("CREATE");
        botonCreate.setBounds(10, 350, 90, 20);
        botonRead = new JButton("READ");
        botonRead.setBounds(110, 350, 90, 20);
        botonUpdate = new JButton("UPDATE");
        botonUpdate.setBounds(210, 350, 90, 20);
        botonDelete = new JButton("DELETE");
        botonDelete.setBounds(310, 350, 90, 20);
        botonAceptar = new JButton("ACEPTAR");
        botonAceptar.setBounds(440, 350, 100, 20);
        botonCancelar = new JButton("CANCELAR");
        botonCancelar.setBounds(560, 350, 100, 20);
        botonSalir = new JButton("SALIR");
        botonSalir.setBounds(560, 10, 100, 20);
        botonGuardar = new JButton("GUARDAR");
        botonGuardar.setBounds(440, 350, 100, 20);
        botonGuardar.setVisible(false);
        botonBorrar = new JButton("BORRAR");
        botonBorrar.setBounds(440, 350, 100, 20);
        botonBorrar.setVisible(false);
        
        botonPrimero = new JButton("<<");
        botonPrimero.setBounds(70, 320, 50, 20);
        botonAnterior =new JButton("<");
        botonAnterior.setBounds(140, 320, 50, 20);
        botonSiguiente = new JButton(">");
        botonSiguiente.setBounds(210, 320, 50, 20);
        botonUltimo = new JButton (">>");
        botonUltimo.setBounds(280, 320, 50, 20);
       
        panel.add(etiId);
        panel.add(etiNombre);
        panel.add(etiDni);
        panel.add(etiEdad);
        panel.add(etiTelefono);
        panel.add(etiObservaciones);
        panel.add(txtId);
        panel.add(txtNombre);
        panel.add(txtEdad);
        panel.add(txtTelefono);
        panel.add(txtObservaciones);
        panel.add(txtDni);
        
        panel.add(botonCreate);
        panel.add(botonUpdate);
        panel.add(botonRead);
        panel.add(botonDelete);
        panel.add(botonAceptar);
        panel.add(botonSalir);
        panel.add(botonCancelar);
        panel.add(botonGuardar);
        panel.add(botonBorrar);
        
        panel.add(botonPrimero);
        panel.add(botonSiguiente);
        panel.add(botonAnterior);
        panel.add(botonUltimo);
                
        panel.add(txtNss);
        panel.add(etiNss);
                
        panel.add(etiImagen);
        
        
        ventana = new JFrame();
        ventana.setTitle("Consulta Medica Privada");
        ventana.setSize(700,420);
        //este metodo devuelve el tamaño de la pantalla
            Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        //obtenemos el tamaño de la ventana
            Dimension window = ventana.getSize();
        //para centrar la ventana lo hacemos con el siguiente calculo
        ventana.setLocation((pantalla.width-window.width)/2, (pantalla.height-window.height)/2);
       
        Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
        teclas.add(AWTKeyStroke.getAWTKeyStroke(
                KeyEvent.VK_ENTER, 0));
        teclas.add(AWTKeyStroke.getAWTKeyStroke(
                KeyEvent.VK_TAB, 0));
        
        // Se pasa el conjunto de teclas al panel principal 
        ventana.getContentPane().setFocusTraversalKeys(
                KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, 
                teclas);
        
        ventana.setDefaultCloseOperation(ventana.ICONIFIED);
        ventana.add(panel);
        
       // ventana.setVisible(true);
    }
    
    public JTextArea getObservaciones(){
        return txtObservaciones;
        
    }
    
    /*
     * Clase para recibir los eventos de tipo Text que se produzcan
     * sobre el objeto TextArea sobre el cual se encuentra registrado
    */
   /* 
    class MiTextListener implements TextListener {
        JTextArea oAreaTexto;

        MiTextListener( JTextArea iAreaTexto ) {
        // Guarda una referencia al objeto TextArea
        oAreaTexto = iAreaTexto;
       }
    /* Se sobrescribe el método textValueChanged() del interfaz
     * TextListener para que indique en la consola el texto que
     * ocupa el área de texto cuando se cambie
    */
    /*    @Override
    /* 
     *Se sobrescribe el método textValueChanged() del interfaz
     * TextListener para que indique en la consola el texto que
     * ocupa el área de texto cuando se cambie
    */ 
   /* public void textValueChanged( TextEvent evt ) {
        System.out.println( oAreaTexto.getText() );
       }
    }*/
         
    

    
    
  /*  public VistaGraficaPaciente(){
        CrearMenuPaciente();
        
    }
   
    
    public static void main(String[] args) {
        VistaGraficaPaciente principal = new VistaGraficaPaciente();
    }*/

}
