package org.ceedcv.ceed150prgpro7.modelo;

/**
 * Fichero: Paciente.java
 *
 * @author Darío Navarro Andrés minombredario@gmail.com
 * 
 */
public class Paciente extends Persona {

    private String nss;
    private int id = 1;
    private static int idsig = 1;
    
    public Paciente(){
    nss = "";
    }
    public Paciente(int id){
    this.id = id;
    }
    
    public Paciente (int id, String nombre,String dni, int edad, int telefono,
              String observaciones, String nss){
          super(nombre, dni, edad, telefono, observaciones);
          this.id = id;
          this.nss =nss;
     
    }

    public static int getIdsig() {
        return idsig;
    }

    public static void setIdsig(int idsig) {
        Paciente.idsig = idsig;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

      
    /**
     * @return the nss
     */
    public String getNss() {
        return nss;
    }

    /**
     * @param nss the nss to set
     */
    public void setNss(String nss) {
        this.nss = nss;
    }

    public String toString() {
        return "" + id ;/*+ "\nPaciente: " + nombre + "\nEdad: " + edad +
                "\nTelefono: " + telefono + "\nObservaciones: " + observaciones;*/
    }

}
